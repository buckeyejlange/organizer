import React, { Component } from 'react';

import Login from './Login';

/**
 * The header of the application.
 */
export class Header extends Component {
    render() {
        return (
            <header>
                <nav>
                    <Login></Login>
                </nav>
            </header>
        );
    }
}