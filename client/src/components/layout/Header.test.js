import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import { Header } from './Header';

describe('Header', () => {
    it('renders without crashing', () => {
        const store = createStore((state = {}, action) => state);
        const div = document.createElement('div');
        ReactDOM.render(<Provider store={store}><Header /></Provider>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});