import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CognitoAuth } from 'amazon-cognito-auth-js';

import authData from '../../environment/cognito.json';
import { authError, signIn, signOut } from '../../store/authentication/actions';

/**
 * Handles signing in and signing out the user.
 */
export class Login extends Component {
    constructor(props) {
        super(props);
        
        this.login = this.login.bind(this);
        this.signOut = this.signOut.bind(this);
        this._loginSuccess = this._loginSuccess.bind(this);
        this._loginFailure = this._loginFailure.bind(this);
        this._initializeAuth = this._initializeAuth.bind(this);
    }

    componentDidMount() {
        this._initializeAuth();
    }

    render() {
        if (this.props.isAuthenticated) {
            return (
                <div>
                    <span>You're logged in! Celebrate</span>
                    <button onClick={this.signOut}>SignOut</button>
                </div>
            );
        }

        return (
            <button onClick={this.login}>Login</button>
        );
    }

    login() {
        this.auth.getSession();
    }

    signOut() {
        this.auth.signOut();
        this.props.dispatch(signOut());
    }

    // TODO: look at putting this into separate service
    _initializeAuth() {
        this.auth = new CognitoAuth(authData);
        this.auth.userhandler = {
            onSuccess: this._loginSuccess,
            onFailure: this._loginFailure
        };

        this.auth.useCodeGrantFlow();

        const currentUrl = window.location.href;
        this.auth.parseCognitoWebResponse(currentUrl);
    }

    _loginSuccess(result) {
        this.props.dispatch(signIn(result));
    }

    _loginFailure(error) {
        this.props.dispatch(authError());
    }
}

function mapStateToProps(state) {
    let authenticated = false;

    if (state.auth) {
        authenticated = state.auth.isAuthenticated;
    }

    return {
        isAuthenticated: authenticated
    }
}

export default connect(mapStateToProps)(Login);