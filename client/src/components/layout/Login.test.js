import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';

import { Login } from './Login';

describe('LoginButton', () => {
    let component = null;
    let lastAction = null;

    beforeEach(() => {
        component = new Login();

        // you're supposed to accomplish this with jest.mock but I can't get
        // it to work right now.
        component.auth = {
            getSession: jest.fn(() => {}),
            signOut: jest.fn(() => {})
        };

        component.props = {
            dispatch: (action) => lastAction = action
        };
    });

    describe('render', () => {
        it('Should render SignOut if user is authenticated.', () => {
            const snapshotComponent = renderer.create(<Login isAuthenticated={true}></Login>);
            let tree = snapshotComponent.toJSON();
            expect(tree).toMatchSnapshot();
        });

        it('Should render Login button if user is not authenticated.', () => {
            const snapshotComponent = renderer.create(<Login isAuthenticated={false}></Login>);
            let tree = snapshotComponent.toJSON();
            expect(tree).toMatchSnapshot();
        });
    });

    describe('login', () => {
        beforeEach(() => {
            component.login();
        });

        it('should get session', () => {
            expect(component.auth.getSession).toHaveBeenCalled();
        });
    });

    describe('signOut', () => {
        beforeEach(() => {
            component.signOut();
        });

        it('should sign the user out', () => {
            expect(component.auth.signOut).toHaveBeenCalled();
        });

        it('should dispatch the SIGN_OUT action', () => {
            expect(lastAction.type).toBe('SIGN_OUT');
        });
    });

    describe('loginSuccess', () => {
        beforeEach(() => {
            component._loginSuccess();
        });

        it('should dispatch SIGN_IN', () => {
            expect(lastAction.type).toBe('SIGN_IN');
        });
    });

    describe('loginFailure', () => {
        beforeEach(() => {
            component._loginFailure();
        });

        it('should dispatch AUTHENTICATION_ERROR', () => {
            expect(lastAction.type).toBe('AUTHENTICATION_ERROR');
        });
    });
});