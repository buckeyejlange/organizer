export function authError() {
    return {
        type: 'AUTHENTICATION_ERROR'
    };
}

export function signIn(tokens) {
    return {
        type: 'SIGN_IN',
        payload: tokens
    };
}

export function signOut() {
    return {
        type: 'SIGN_OUT'
    };
}