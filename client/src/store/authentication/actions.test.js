import { authError, signIn, signOut } from "./actions";

describe('AuthenticationActions', () => {
    describe('authError', () => {
        it('should create a type of AUTHENTICATION_ERROR', () => {
            expect(authError().type).toBe('AUTHENTICATION_ERROR');
        });
    });

    describe('signIn', () => {
        it('should create a type of SIGN_IN', () => {
            expect(signIn({ access_tokens: '' }).type).toBe('SIGN_IN');
        });

        it('should set the payload to the tokens', () => {
            expect(signIn({ access_tokens: 'test' }).payload).toEqual({ access_tokens: 'test' });
        });
    });

    describe('signOut', () => {
        it('should create a type of SIGN_OUT', () => {
            expect(signOut().type).toBe('SIGN_OUT');
        });
    });
});