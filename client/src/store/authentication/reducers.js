export default function authenticationReducer(state = {}, action) {
    if (action.type === 'SIGN_IN') {
        return {
            isAuthenticated: true,
            access_tokens: action.payload
        }
    } else if (action.type === 'AUTHENTICATION_ERROR' || action.type === 'SIGN_OUT') {
        return {
            isAuthenticated: false,
            access_tokens: null
        }
    }

    return state;
}