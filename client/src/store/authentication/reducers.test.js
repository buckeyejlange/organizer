import authenticationReducer from './reducers';

describe('authenticationReducer', () => {
    describe('SIGN_IN', () => {
        const action = {
            type: 'SIGN_IN',
            payload: {
                access_token: 'test'
            }
        };

        it('should return isAuthenticated = true', () => {
            const result = authenticationReducer({}, action);
            expect(result.isAuthenticated).toBeTruthy();
        });

        it('should add payload to access_tokens', () => {
            const result = authenticationReducer({}, action);
            expect(result.access_tokens.access_token).toBe('test');
        });

        it('should not mutate state', () => {
            const existingState = { isAuthenticated: false, access_tokens: null };
            const result = authenticationReducer(existingState, action);
            expect(existingState.isAuthenticated).toBeFalsy();            
            expect(existingState.access_tokens).toBeNull();
        });
    });

    describe('AUTHENTICATION_ERROR', () => {
        const action = {
            type: 'AUTHENTICATION_ERROR',
            payload: null
        };

        it('should return isAuthenticated = false', () => {
            const result = authenticationReducer({}, action);
            expect(result.isAuthenticated).toBeFalsy();
        });

        it('should not add access_tokens', () => {
            const result = authenticationReducer({}, action);
            expect(result.access_tokens).toBeNull();
        });

        it('should not mutate state', () => {
            const existingState = { isAuthenticated: true, access_tokens: { access_token: 'test' } };
            const result = authenticationReducer(existingState, action);
            expect(existingState.isAuthenticated).toBeTruthy();            
            expect(existingState.access_tokens.access_token).toBe('test');
        });
    });

    describe('SIGN_OUT', () => {
        const action = {
            type: 'SIGN_OUT',
            payload: null
        };

        it('should return isAuthenticated = false', () => {
            const result = authenticationReducer({}, action);
            expect(result.isAuthenticated).toBeFalsy();
        });

        it('should not add access_tokens', () => {
            const result = authenticationReducer({}, action);
            expect(result.access_tokens).toBeNull();
        });

        it('should not mutate state', () => {
            const existingState = { isAuthenticated: true, access_tokens: { access_token: 'test' } };
            const result = authenticationReducer(existingState, action);
            expect(existingState.isAuthenticated).toBeTruthy();            
            expect(existingState.access_tokens.access_token).toBe('test');
        });
    });

    describe('Not matching', () => {
        it('should return passed in state', () => {
            const result = authenticationReducer({ test: 'test' }, { type: '' });
            expect(result.test).toBe('test');
        });
    });
});