const Api = require('claudia-api-builder');

const api = new Api();

// TODO: Consider breaking out into separate file
api.registerAuthorizer('userAuthentication', {
    providerARNs: [ '' ]
});

api.get('/', () => 'Public API');

api.get('/restricted', 
    () => 'Restricted API',
    {
        cognitoAuthorizer: 'userAuthentication'
    });

api.get('/unrestricted', () => 'Unrestricted');

module.exports = api;